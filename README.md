# Primes counter
```shell
go build
./go-primes-counter -start 10 -max 1000 -print-count -1 -step-size 100
```

## Usage
```text
$ ./go-primes-counter -h
Usage of ./go-primes-counter:
  -max int
    	The end value (default 10000)
  -print-count int
    	Number of primes to print. -1 to print all.
    	Primes are not guaranteed to be sorted. (default 20)
  -start int
    	The start number
  -step-size int
    	The job size (default 1000)
```
# Reuse
If you do reuse my work, please consider linking back to this repository 🙂